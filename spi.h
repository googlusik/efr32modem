#ifndef spi_h__
#define spi_h__
#include "spidrv.h"
#include "bsp.h"
#include "gpiointerrupt.h"
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "em_core.h"
#include "uartdrv.h"
#include "spidrv/inc/spidrv.h"
#include "hal_common.h"
#include "const.h"

void SetupSPI();
void SpiIntCallback();
unsigned int GetPacketForSend(unsigned char **pPacket);
bool SendSpiData(unsigned char*pData, unsigned int count);
typedef enum { CC1310_EV_NONE = 0,  CC1310_EV_SEND_CONFIRMATION = 1, CC1310_EV_RECV_RAW_PACKET = 2 } SI_EVENTS;
void SetSpiEvent(SI_EVENTS ev);





#endif // spi_h__
