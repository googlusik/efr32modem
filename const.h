#ifndef const_h__
#define const_h__

/* Commands */
#define CC1310_CMD_READ_EVENTS 		0x01
#define CC1310_CMD_SEND_RAW_PACKET 	0x02
#define CC1310_CMD_GET_ID           0x03
#define CC1310_CMD_SET_MAC          0x04
#define CC1310_CMD_GET_RX_LEN		0x05
#define CC1310_CMD_READ_RX_DATA		0x06
#define CC1310_CMD_RX_START         0x07

#define USART_INIT_NMEA \
{ \
	BSP_BCC_USART,                                     /* USART port */ \
	115200,                                            /* Baud rate */  \
	21 /*BSP_BCC_TX_LOCATION*/,                               /* USART Tx pin location number */  \
	25,                               /* USART Rx pin location number */  \
	(USART_Stopbits_TypeDef)USART_FRAME_STOPBITS_ONE,  /* Stop bits */  \
	(USART_Parity_TypeDef)USART_FRAME_PARITY_NONE,     /* Parity */     \
	(USART_OVS_TypeDef)USART_CTRL_OVS_X16,             /* Oversampling mode*/      \
	false,                                             /* Majority vote disable */ \
	uartdrvFlowControlNone,                            /* Flow control */          \
	BSP_BCC_TXPORT,                                    /* CTS port number */       \
	BSP_BCC_TXPIN,                                     /* CTS pin number */        \
	BSP_BCC_RXPORT,                                    /* RTS port number */       \
	BSP_BCC_RXPIN,                                     /* RTS pin number */        \
	(UARTDRV_Buffer_FifoQueue_t *)&UartRxQueue,                                              /* RX operation queue */    \
	(UARTDRV_Buffer_FifoQueue_t *)&UartTxQueue         /* TX operation queue           */      \
}

#define  SPIDRV_SLAVE \
{ \
        USART1, \
        16, /* USART Tx pin location number     */    \
        14, /* USART Rx pin location number     */    \
        8,/* USART Clk pin location number    */    \
        5, /* USART Cs pin location number     */    \
        0,                            /* Bitrate                          */    \
        8,                            /* Frame length                     */    \
        0xDA,                            /* Dummy tx value for rx only funcs */    \
        spidrvSlave,                  /* SPI mode                         */    \
        spidrvBitOrderMsbFirst,       /* Bit order on bus                 */    \
        spidrvClockMode3,             /* SPI clock/phase mode             */    \
        spidrvCsControlAuto,          /* CS controlled by the driver      */    \
        spidrvSlaveStartImmediate     /* Slave start transfers immediately*/    \
};

#define SPI_INT_PORT gpioPortB
#define SPI_INT_PIN 13
#define APP_MAX_PACKET_LENGTH 250
#define CS_INT_PORT gpioPortB
#define CS_INT_PIN 14
#define LED_PORT gpioPortF
#define LED_PIN 2
#define LED_PORTGREEN gpioPortF
#define LED_PINGREEN  3
#define LED_PINEMPTY  16
#define MAX_BUFFER_SIZE 255
#define MAX_RF_SENDING_SIZE 255

#endif // const_h__
