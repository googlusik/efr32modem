#include "utils.h"
#include "const.h"
#include "em_gpio.h"
// no comment
void nop_delay()
{
	static volatile int ii = 0; // avoid optimize
	long int i;
	for (i = 0; i < 10000L; i++)
		ii++;
}
void nop_delay2()
{
	static volatile int ii = 0; // avoid optimize
	long int i;
	for (i = 0; i < 2L; i++)
		ii++;
}
//
void PulseIntDelay()
{
	//GPIO_PinOutSet(CS_INT_PORT, CS_INT_PIN);
	nop_delay();
	//GPIO_PinOutClear(CS_INT_PORT, CS_INT_PIN);


}
void PulseIntDelay2()
{
	GPIO_PinOutSet(CS_INT_PORT, CS_INT_PIN);
	nop_delay2();
	GPIO_PinOutClear(CS_INT_PORT, CS_INT_PIN);


}
