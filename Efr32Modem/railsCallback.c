#include <stdint.h>
#include "const.h"
#include "rail.h"
#include "rail_types.h"
#include "em_gpio.h"
#include "em_int.h"
#include "packetizer.h"
#include "railscallback.h"

#define CRC16_POLY 0x8005
#define CRC_INIT 0xFFFF
extern uint16_t culCalcCRC(uint8_t crcData, uint16_t crcReg);



unsigned char receiveBuffer[MAX_BUFFER_SIZE];
static char rx[MAX_RF_SENDING_SIZE];
volatile static bool isTxDone = true;
volatile static char isReceive = false;
unsigned int _channel = 26;
static volatile char txProcessing = false;
volatile bool rxNewPacket = false;
volatile int rxCount = 0;
uint32_t INT_LockCnt = 0;

volatile int sent = 0;
volatile int started = 0;

void StartReceive(unsigned int channel)
{
	isReceive = true;
	_channel = channel;
	RAIL_RfIdleExt(RAIL_IDLE, true);
	RAIL_RxStart(_channel);
}
//
int rfReceivePacket(char *pdata)
{	
	if (rxNewPacket)
	{
		if (pdata != NULL)
			memcpy(pdata, receiveBuffer, rxCount);
		return rxCount;
	}
	else return 0;
}
//
void ReadRxPacket()
{
	rxNewPacket = false;
}
//
bool isReceiveMode()
{
	return isReceive;
}

// cant interrupt TX if processing
bool IsTxDone()
{
	char temp;
	temp = isTxDone;
	return temp;
}
//
void StartTx(unsigned int channel)
{
	_channel = channel;
	while (!IsTxDone())
		;
	txProcessing = true;
	isReceive = false;
	RAIL_RfIdleExt(RAIL_IDLE, true);
	RAIL_CsmaConfig_t csmaConfig = RAIL_CSMA_CONFIG_802_15_4_2003_2p4_GHz_OQPSK_CSMA;
	 RAIL_TxOptions_t txOption;
	 txOption.waitForAck = false;
	     txOption.removeCrc  = false;
	     txOption.syncWordId = 0;
	//RAIL_TxStartWithOptions(channel, &txOption, RAIL_CcaCsma, &csmaConfig);

	RAIL_TxStart(channel,  RAIL_CcaCsma, &csmaConfig);
	//RAIL_TxStartWithOptions()
	//RAIL_TxOptions_t
	started++;
}

// for RAIL internal use, do not touch
void *RAILCb_AllocateMemory(uint32_t size)
{
	return receiveBuffer;
}

void *RAILCb_BeginWriteMemory(void *handle,
	uint32_t offset,
	uint32_t *available)
{
	return ((uint8_t*)handle) + offset;
}

void RAILCb_EndWriteMemory(void *handle, uint32_t offset, uint32_t size)
{
	// Do nothing
}

void RAILCb_FreeMemory(void *ptr)
{
}

void RAILCb_RadioStateChanged(uint8_t state)
{
}

void RAILCb_RfReady()
{
}

void RAILCb_TimerExpired()
{
}

void RAILCb_CalNeeded()
{
}

void RAILCb_RxRadioStatus(uint8_t status)
{
	// error here
	RAIL_RxStart(_channel);
}


void RAILCb_TxPacketSent(RAIL_TxPacketInfo_t *txPacketInfo)
{
	txProcessing = false;
	isTxDone = true;
	sent++;
	RAIL_SetFixedLength(MAX_RF_SENDING_SIZE);
	RAIL_RxStart(_channel);
}
uint16_t chsum = CRC_INIT;
volatile int errs = 0;
void RAILCb_RxPacketReceived(void *rxPacketHandle)
{
	RAIL_RxPacketInfo_t *rxPacketInfo;
	int8_t RssiValue = 0;
	// Do nothing if we had no memory to capture the packet
	rxPacketInfo = (RAIL_RxPacketInfo_t*)rxPacketHandle;
	if (rxPacketInfo == NULL) {
		return;
	}
	int len = rxPacketInfo->dataLength; //length extraxt
	int decoded = 0;
	int totalDecoded = 0;

//	memcpy(rx, rxPacketInfo->dataPtr + 1 /*skip length byte*/, MAX_RF_SENDING_SIZE);
	//decoded = decodeVarLenMsgAdv(rxPacketInfo->dataPtr + 1 /*skip length byte*/,
		//rxPacketInfo->dataPtr + 1 /*skip length byte*/,
//		len - 1,/*skip length byte*/
//		&totalDecoded);
		uint8_t dataLen = rxPacketInfo->dataPtr[0];
		uint8_t *preceived = rxPacketInfo->dataPtr + 1;
		memcpy(rx, preceived /*skip length byte*/, MAX_RF_SENDING_SIZE);



	 	uint16_t i;
	 	chsum = CRC_INIT;
	 	for (i = 0; i < dataLen+1+2/*with len byte*/;i++)
	 	{
	 		chsum = culCalcCRC(rxPacketInfo->dataPtr[i], chsum);
	 	}
	 	if (chsum != 0)
	 	{
	 		errs++;
	 	}


	//if (decoded != -1) // succesfully decoded RS
		if (chsum == 0)
	{
		if (!rxNewPacket) // if already read prev
		{
			MakeSpiPacketFromRadio(receiveBuffer, rxPacketInfo->dataPtr + 1 /*no len header*/, dataLen, rxPacketInfo->appendedInfo.rssiLatch);
			//MakeSpiPacketFromRadio(receiveBuffer, rxPacketInfo->dataPtr + 1 /*no len header*/, totalDecoded, rxPacketInfo->appendedInfo.rssiLatch);
			rxCount = dataLen-8;
			rxNewPacket = true;			
		}

	}
	StartReceive(_channel);// start receive again

}
volatile uint8_t txstat = 0;
void RAILCb_TxRadioStatus(uint8_t status)
{
	txstat = status;
}
