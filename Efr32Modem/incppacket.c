#define _GNU_SOURCE 1

//#include <features.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
//#include <arpa/inet.h>
//#include <db.h>

//#include "incpgnssd.h"
#include <inttypes.h>
#include "incppacket.h"
#include "utils.h"


uint16_t htons(uint16_t v) {
  return (v >> 8) | (v << 8);
}
uint32_t htonl(uint32_t v) {
  return htons(v >> 16) | (htons((uint16_t) v) << 16);
}
uint64_t  hton64 (uint64_t v)
{
	uint64_t res = 0;
	uint64_t  t;
	t = (v >> 56ULL) & 0xff; res |= t << 0ULL;
	t = (v >> 48ULL) & 0xff; res |= t << 8ULL;
	t = (v >> 40ULL) & 0xff; res |= t << 16ULL;
	t = (v >> 32ULL) & 0xff; res |= t << 24ULL;
	t = (v >> 24ULL) & 0xff; res |= t << 32ULL;
	t = (v >> 16ULL) & 0xff; res |= t << 40ULL;
	t = (v >> 8ULL) & 0xff; res |= t << 48ULL;
	t = (v >> 0ULL) & 0xff; res |= t << 56ULL;
	return res;
}


uint16_t ntohs(uint16_t v) {
  return htons(v);
}
uint32_t ntohl(uint32_t v) {
  return htonl(v);
}

static uint32_t packet_id_by_packet_type[256];

static void __incp_header_to_network(struct incp_header *ih, char *buf) {

	buf[0] = (ih->version << 4) | ((ih->length & 0x0F00) >> 8);
	buf[1] = ih->length & 0xFF;
	buf[2] = ih->packet_type;
	buf[3] = ih->hops;

	uint32_t *id_ptr = (uint32_t *) &buf[4];
	*id_ptr = htonl(ih->id);

	buf[8] = ih->area_id;
	buf[9] = ih->broadcast == true ? ih->device_type | 0x80 : ih->device_type;
	buf[10] = ih->tx_power;
	buf[11] = ih->rx_power;

	uint16_t *mac_high = (uint16_t *) &buf[12];
	*mac_high = htons( ( ih->mac >> 32 ) & 0xFFFF );

	uint32_t *mac_low = (uint32_t *) &buf[14];
	*mac_low = htonl( ih->mac & 0xFFFFFFFF );

}
static int id = 0;
int32_t incp_header_to_network(int packet_type, int packet_length, char *buf) {

	struct incp_header ih;

	ih.version = 1;
	ih.device_type = DT_GNSSTAG;
	ih.hops = 0;
	ih.broadcast = true;
	ih.area_id = 0;
	ih.packet_type = packet_type;
	ih.id = id++;
	ih.rx_power = 20;
	ih.tx_power = 10;
	ih.mac = 0x666666666665;
	ih.length = packet_length;

	__incp_header_to_network(&ih, buf);

	return ih.id;

}

int incp_alive_config7_to_network(int alive_interval, char *buf) {

	buf[0] = 0xA;
	buf[1] = 7;

	uint32_t *cycle_period_ptr = (uint32_t *) &buf[2];

	*cycle_period_ptr = htonl(alive_interval);

	uint32_t *local_time_ptr = (uint32_t *) &buf[6];

	struct timeval now;

	if (gettimeofday(&now, NULL) == 0) {

		*local_time_ptr = htonl(now.tv_sec);

	} else {

		*local_time_ptr = 0;

	}

	return 10;

}
uint8_t gpsCfg17[10] = { 0x0a, 0x07, 0x00, 0x00, 0x03, 0xe8, 0x00, 0x00, 0x00, 0x55 };
/*
#define SIZEOF_BLOCK_19_MEMBER		18
int incp_alive_config19_to_network(db_struct_t** msg, char* buf, int cnt)
{
	buf[0] = cnt *SIZEOF_BLOCK_19_MEMBER + 2;
	buf[1] = 19;
	int n = 0;
	while (cnt--)
	{
		DEBUG_PRINT(DEBUG_LEVEL_INFO,"From BD  latt = %d, long= %d  \n", msg[n]->data.latt, msg[n]->data.longt);
		//uint64_t* p = (uint64_t*)&buf[2 + n*18];
		*((uint64_t*)&buf[2 + n * SIZEOF_BLOCK_19_MEMBER]) = hton64(msg[n]->data.time);
		*((uint32_t*)&buf[10 + n * SIZEOF_BLOCK_19_MEMBER]) = htonl(msg[n]->data.latt);
		*((uint32_t*)&buf[14 + n * SIZEOF_BLOCK_19_MEMBER]) = htonl(msg[n]->data.longt);
		*((uint16_t*)&buf[18 + n * SIZEOF_BLOCK_19_MEMBER]) = htons(msg[n]->data.alt);
		n++;
	}

	return n * SIZEOF_BLOCK_19_MEMBER + 2;
}
*/

void incp_append_to_buffer(char** ppBuf, void* data, int len)
{
	if (len == 2)
	{
		uint16_t ui16 = htons(*(uint16_t*)data);
		memcpy(*ppBuf, &ui16, sizeof(ui16));
		*ppBuf += sizeof(ui16);
	}
	if (len == 4)
	{
		uint32_t ui = htonl(*(uint32_t*)data);
		memcpy(*ppBuf, &ui, sizeof(ui));
		*ppBuf += sizeof(ui);
	}
	if (len == 8)
	{
		uint64_t ui64 = hton64(*(uint64_t*)data);
		memcpy(*ppBuf, &ui64, sizeof(ui64));
		*ppBuf += sizeof(ui64);
	}

}


