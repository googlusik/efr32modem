#include <stdint.h>
#include <stdbool.h>

//#include "bdb.h"

#define INCP_HEADER_SIZE 		18
#define GNSS18_BLOCK_SIZE		38
/**
 * Ð¢Ð¸Ð¿Ñ‹ ÑƒÑ�Ñ‚Ñ€Ð¾Ð¹Ñ�Ñ‚Ð² INCP (Device types)
 */
#define DT_GNSSTAG 8

/**
 * Ð¢Ð¸Ð¿Ñ‹ Ð¿Ð°ÐºÐµÑ‚Ð¾Ð² INCP(Packet types)
 */
#define PT_ALIVE 0x20

/**
 * Ð¢Ð¸Ð¿Ñ‹ Ð±Ð»Ð¾ÐºÐ¾Ð² ALIVE INCP(Alive block types)
 */
#define ABT_CONFIG7 7

struct incp_header {

	uint8_t version;
	uint16_t length;
	uint8_t packet_type;
	uint8_t hops;
	uint32_t id;
	uint8_t area_id;
	uint8_t device_type;
	bool broadcast;
	uint8_t tx_power;
	uint8_t rx_power;
	uint64_t mac;

};

struct alive_config7_block {

	uint32_t cycle_period;
	uint32_t local_time;

};

int32_t incp_header_to_network(int packet_type, int length, char *buf);

int incp_alive_config7_to_network(int alive_interval, char *buf);
void incp_append_to_buffer(char** ppBuf, void* data, int len);
extern uint8_t gpsCfg17[];
//int incp_alive_config19_to_network(db_struct_t** msg, char* buf, int cnt);

