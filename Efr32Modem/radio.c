#include "radio.h"
#include "const.h"
#include "rail_config.h"

static int currentConfig = 0; //default is first in list

static const RAIL_Init_t railInitParams = {
	APP_MAX_PACKET_LENGTH,
	RADIO_CONFIG_XTAL_FREQUENCY,
	RAIL_CAL_ALL
};


void InitRadio()
{
	RAIL_RfInit(&railInitParams);
	// Configure RAIL callbacks with no appended info
	RAIL_RxConfig((RAIL_RX_CONFIG_INVALID_CRC
		| RAIL_RX_CONFIG_SYNC1_DETECT
		| RAIL_RX_CONFIG_ADDRESS_FILTERED
		| RAIL_RX_CONFIG_BUFFER_OVERFLOW),
		true);
}

void changeRadioConfig(int newConfig)
{
	// Turn off the radio before reconfiguring it
	RAIL_RfIdleExt(RAIL_IDLE, true);

	// Reconfigure the radio parameters
	RAIL_PacketLengthConfigFrameType(frameTypeConfigList[newConfig]);
	if (RAIL_RadioConfig((void*)configList[newConfig])) { while (1); }

	// Set us to a valid channel for this config and force an update in the main
	// loop to restart whatever action was going on
	RAIL_ChannelConfig(channelConfigs[newConfig]);
	currentConfig = newConfig;
}

