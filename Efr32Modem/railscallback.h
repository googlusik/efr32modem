#ifndef railscallback_h__
#define railscallback_h__
bool IsTxDone();
void StartReceive(unsigned int channel);
bool isReceiveMode();
int rfReceivePacket(char *pdata);
void ReadRxPacket();
void StartTx(unsigned int channel);

#endif // railscallback_h__
