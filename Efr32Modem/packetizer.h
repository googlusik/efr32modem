#ifndef packetizer_h__
#define packetizer_h__
#include "minmea/minmea.h"
int MakeRadioPacketFromSpi(unsigned char *packet, unsigned char *dataTx, unsigned int toSendCnt);
int MakeSpiPacketFromRadio(unsigned char *packet, unsigned char *pRaw, unsigned int len, unsigned char rssi);
int MakeSpiNmeaPacketFromRmcframe(unsigned char *packet, struct minmea_sentence_rmc frame);
#endif // packetizer_h__
