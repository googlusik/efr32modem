#include "RS/dasmRS.h"
#include "packetizer.h"
#include "incppacket.h"
#include <string.h>
static int id = 1;
extern char mac[6];
//returns total radio bytes
// some dirty binary stuff, because radio packet is weak documented
#define CRC16_POLY 0x8005
#define CRC_INIT 0xFFFF
uint16_t culCalcCRC(uint8_t crcData, uint16_t crcReg) {
	uint8_t i;
	 for (i = 0; i < 8; i++)
	 {
		 if (((crcReg & 0x8000) >> 8) ^ (crcData & 0x80))
			 crcReg = (crcReg << 1) ^ CRC16_POLY;
		 else
			 crcReg = (crcReg << 1);
		 crcData <<= 1;
	 }
	 return crcReg;
}// culCalcCR

int MakeRadioPacketFromSpi(unsigned char *packet, unsigned char *dataTx, unsigned int toSendCnt)
{
	char ofs = 1; // offset for Length byte
	uint16_t chsum ;
	memcpy(&packet[ofs + 3], dataTx, 6);
	memcpy(&packet[ofs + 9], mac, 6);
	memcpy(&packet[ofs + 15], &dataTx[6], toSendCnt - 6);	
	packet[ofs + 0] = dataTx[5]; // I do not know why, it is taken from PTZ sources
	packet[ofs + 1] = id >> 8;
	packet[ofs + 2] = id;
	id++;
	int sendLen;
	sendLen = (toSendCnt - 6) + 3 + 12;
	packet[0] = sendLen; /*+LEN*/;// first radio packe byte must be LEN
 	chsum = CRC_INIT;
 	uint16_t i;
 	for (i = 0; i < sendLen+1/*with len byte*/;i++)
 	{
 		chsum = culCalcCRC(packet[i], chsum);
 	}

 	packet[sendLen+1] = chsum >> 8;
 	packet[sendLen+2] = chsum;

	//int toSend = 0;
	//toSend = encodeVarLenMsgAdv(&packet[1], &packet[1], sendLen);

	return sendLen+3; /*plus CRC and len byte itself*/
}
int MakeSpiPacketFromRadio(unsigned char *packet, unsigned char *pRaw, unsigned int len, unsigned char rssi)
{
	memcpy(packet, pRaw + 9 /*skip header*/, 6);
	memcpy(&packet[7], pRaw  + 15 /*skip header*/, len - 15);
	packet[6] = rssi;
	return 0;
}
// 
int MakeSpiNmeaPacketFromRmcframe(unsigned char *packet, struct minmea_sentence_rmc frame)
{
	static int gpsid = 1;
	unsigned int len = 0;
	struct incp_header *ih;
	memset(packet, 0xff, 6);
	unsigned char *cur = &packet[6];
	incp_header_to_network(0x20 /*alive*/, 0x42 /*int packet_length*/, cur);
	cur += 18;	
	gpsCfg17[10] = gpsid++;
	memcpy(cur, gpsCfg17, 10);
	cur += 10;
	*cur++ = GNSS18_BLOCK_SIZE;
	// block code
	*cur++ = 18;

	struct timespec ts;
	uint64_t time_stamp;
	if (minmea_gettime(&ts, &frame.date, &frame.time) == 0)
	{
		time_stamp = ts.tv_sec * 1000ULL + (ts.tv_nsec / 1000000ULL);
	}
	else
	{
		time_stamp = 0;
	}
	uint64_t ntime = hton64(time_stamp);
	memcpy(cur, &ntime, sizeof(ntime));
	cur += sizeof(ntime);
	//////////double l = minmeaGetLatt();      incp_append_to_buffer(&cur, &l, sizeof(l));
	double l = ((double)minmea_tocoord(&frame.latitude));
	incp_append_to_buffer(&cur, &l, sizeof(l));		
	l = (minmea_tocoord(&frame.longitude));	
	incp_append_to_buffer(&cur, &l, sizeof(l));
	static int yf = 0;
	float f = (minmea_tocoord(&frame.speed)); // alt	
	incp_append_to_buffer(&cur, &f, sizeof(f));
	f = (minmea_tocoord(&frame.speed)); // speed
	incp_append_to_buffer(&cur, &f, sizeof(f));
	f = (minmea_tocoord(&frame.speed)); //cours
	incp_append_to_buffer(&cur, &f, sizeof(f));
	/*		f = htonl(minmeaGetAlt());
	memcpy(cur, &f, sizeof(f));
	cur += sizeof(f);
	*/
	//memcpy (&dataTx[6], gpsData, sizeof (gpsData));
	len = cur - packet;

	return len;
}
