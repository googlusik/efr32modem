#include "spi.h"
#include "incppacket.h"
#include <string.h>
#include "const.h"


const char modem_devid[] = { 0xde, 0xad, 0xba, 0xbe };
//
typedef enum {
	WAIT_CMD,
	WAIT_CMD_LEN,
	WAIT_DATA_LEN,
	REC_LEN,
	TX,
	RX_MAC,
	RX_DATA,
	STATE_UNK,
} HANLDER_STATE;
//
static HANLDER_STATE state = WAIT_CMD;


static char curCmd;
unsigned char mac[6] = {1,2,3,4,5,6};
unsigned int countRecBytesFromSpi = 0;
volatile bool newPacketFromSpi = 0;
static char spiBuffer[256];
unsigned  char receivedSpiData[256];
bool readyForNewPacket = true;

SPIDRV_HandleData_t SpiHandleData;
SPIDRV_Handle_t spiHandle = &SpiHandleData;
static USART_TypeDef *spi = USART1;
static char rxBuf[512];
static char TxBuf[512];
static int iRx = 0;
static int iTx = 0;
static int spiTxDataLength = 0;
static int toTx = 0;

SI_EVENTS siEvent = CC1310_EV_NONE;
// returns false if busy
bool SendSpiData(unsigned char*pData, unsigned int count)
{
	if (!readyForNewPacket)
		return false;
	if (count > sizeof(TxBuf))
		return false;
	memcpy(spiBuffer, pData, count);
	spiTxDataLength = count;
	readyForNewPacket = false;
	return true;
}
//
void SetSpiEvent(SI_EVENTS ev)
{
	siEvent |= ev;
}
SI_EVENTS GetSpiEvent()
{
	return siEvent;
}
void ClearSpiEvents()
{
	siEvent = CC1310_EV_NONE;
}
// hardware pin intterupt, when NSS asserted
void SpiIntCallback()
{
	state = WAIT_CMD;
}
// begin SPI transmission
static void StartSpiTransfer()
{
	iTx = 0;
	spi->TXDATA = TxBuf[iTx++];
	toTx--;
	state = TX;
	NVIC_ClearPendingIRQ(USART1_TX_IRQn);
	NVIC_EnableIRQ(USART1_TX_IRQn);
}
// andrew's  protokol - unrollled loop
void SpiByteHandler(char d)
{	
	switch (state)
	{
	case WAIT_CMD:
		curCmd = d;
		if (curCmd == CC1310_CMD_GET_ID) 
		{
			state = WAIT_CMD_LEN;
		}
		else if (curCmd == CC1310_CMD_SET_MAC)
		{
			state = WAIT_DATA_LEN;
		}
		else if (curCmd == CC1310_CMD_RX_START)
		{
			state = WAIT_DATA_LEN;
		}
		else if (curCmd == CC1310_CMD_SEND_RAW_PACKET)
		{
			state = WAIT_DATA_LEN;
		}
		else if (curCmd == CC1310_CMD_READ_EVENTS)
		{
			state = WAIT_CMD_LEN;
		}
		else if (curCmd == CC1310_CMD_GET_RX_LEN)
		{
			state = WAIT_CMD_LEN;
		}
		else if (curCmd == CC1310_CMD_READ_RX_DATA)
		{
			state = WAIT_CMD_LEN;
		}

		break;
		/////////////////////////////////////////
	case WAIT_CMD_LEN:
		if ((curCmd == CC1310_CMD_GET_ID) && (d == 1))
		{
			state = WAIT_DATA_LEN;
		}
		else if (curCmd == CC1310_CMD_READ_EVENTS && (d == 1))
		{
			state = WAIT_DATA_LEN;
		}
		else if (curCmd == CC1310_CMD_GET_RX_LEN && (d == 1))
		{
			state = WAIT_DATA_LEN;
		}
		else if (curCmd == CC1310_CMD_READ_RX_DATA && (d == 1))
		{
			state = WAIT_DATA_LEN;
		}
		else
		{
			state = WAIT_CMD; 
		}
		break;
		///////////////////////////
	case WAIT_DATA_LEN:
		if ((curCmd == CC1310_CMD_GET_ID) && (d == 4))
		{
			memcpy(TxBuf, modem_devid, 4);
			toTx = 4; 
			iTx = 0;
			spi->TXDATA = TxBuf[iTx++];
			toTx--;
			state = TX;
			NVIC_ClearPendingIRQ(USART1_TX_IRQn);
			NVIC_EnableIRQ(USART1_TX_IRQn);
		}
		else if (curCmd == CC1310_CMD_READ_EVENTS && (d == 4))
		{

			TxBuf[0] = GetSpiEvent();
			ClearSpiEvents();
			TxBuf[1] = 0;
			TxBuf[2] = 0;
			TxBuf[3] = 0;
			toTx = 4;
			StartSpiTransfer();
		}
		else if (curCmd == CC1310_CMD_GET_RX_LEN && (d == 4))
		{

			TxBuf[0] = spiTxDataLength;
			TxBuf[1] = 0;
			TxBuf[2] = 0;
			TxBuf[3] = 0;
			toTx = 4;
			StartSpiTransfer();
		}
		else if (curCmd == CC1310_CMD_READ_RX_DATA )
		{
			readyForNewPacket = false;
			memcpy(TxBuf, spiBuffer, d);
			toTx = d; // TODO range check
			StartSpiTransfer();
		}

		else if ((curCmd == CC1310_CMD_SET_MAC) && (d == 6))
		{
			state = RX_MAC;
			iRx = 0;
		}
		else if ((curCmd == CC1310_CMD_RX_START) && (d == 0))
		{
			state = WAIT_CMD; 			
		}
		else if ((curCmd == CC1310_CMD_SEND_RAW_PACKET))
		{
			if (d != 0)
			{
				state = RX_DATA;
				iRx = 0;
				countRecBytesFromSpi = d;
			}
			else
			{
				state = WAIT_CMD;
			}
		}
		else
		{			// WTF
			state = WAIT_CMD;
		}		
		break;
	case RX_MAC:
		rxBuf[iRx++] = d;
		if (iRx == sizeof(mac))
		{
			memcpy(mac, rxBuf, sizeof(mac));
			state = WAIT_CMD;
		}
		break;
	case RX_DATA:
		rxBuf[iRx++] = d;
		if (iRx == countRecBytesFromSpi)
		{			
			memcpy(receivedSpiData, rxBuf, countRecBytesFromSpi);
			newPacketFromSpi = true;
			state = WAIT_CMD;
		}
	default:
		break;
	}
}
//
void USART1_RX_IRQHandler(void)
{
	if (spi->STATUS & USART_STATUS_RXDATAV)
	{
		/* Reading out data */
		char d = spi->RXDATA;
		SpiByteHandler(d);
	}
}

void USART1_TX_IRQHandler(void)
{
	static char t = '0';
	USART_TypeDef *spi = USART1;
	if (spi->STATUS & USART_STATUS_TXBL)
	{
		if (toTx != 0)
		{
			spi->TXDATA = TxBuf[iTx++];
			toTx--;
		}
		else
		{
			// end TX
			spi->TXDATA = t++;
			if (state == TX)
			{
				readyForNewPacket = true;
				state = WAIT_CMD;
			}
			NVIC_DisableIRQ(USART1_TX_IRQn);
		}
	}

}
//
void SetupSPI ()
{
	SPIDRV_Init_t spiInit = SPIDRV_SLAVE;
	SPIDRV_Init(spiHandle, &spiInit);
	toTx = 0; iTx = 0; iRx = 0;
	spiInit.port->CMD = USART_CMD_CLEARRX;
	spiInit.bitRate = 4000000UL;
	/* Enable interrupts */
	NVIC_ClearPendingIRQ(USART1_RX_IRQn);
	NVIC_EnableIRQ(USART1_RX_IRQn);
	spiInit.port->IEN |= USART_IEN_RXDATAV;
	/* Clear TX */
	spiInit.port->CMD = USART_CMD_CLEARTX;
	NVIC_ClearPendingIRQ(USART1_TX_IRQn);
	spiInit.port->IEN |= USART_IEN_TXBL;
	NVIC_EnableIRQ(USART1_TX_IRQn);
	GPIO_IntConfig(SPI_INT_PORT, SPI_INT_PIN, false /*risingEdge*/, true /*fallingEdge*/, true);	
	GPIOINT_CallbackRegister(SPI_INT_PIN, SpiIntCallback);
	GPIO_IntEnable(1 << 21); // PB13 NSS PIN21
	SPIDRV_SetBitrate(spiHandle,  4000000UL);
//	
}

unsigned int GetPacketForSend(unsigned char **ppPacket)
{
	if (newPacketFromSpi)
	{		
		newPacketFromSpi = false;
		*ppPacket = receivedSpiData;
		return countRecBytesFromSpi;
	}
	return 0;
}
