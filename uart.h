#ifndef DASMUART_H_
#define DASMUART_H_
#include "uartdrv.h"
#include "minmea/minmea.h"
void SetupUART ();
int NMEAProcess (struct minmea_sentence_rmc *frame);
extern UARTDRV_Handle_t      UARTHandle;

#endif /* DASMUART_H_ */
