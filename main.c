/***************************************************************************//**
 * @file main.c
 * @brief This application allows the user to use Button 1 to switch between
 * modes (Duty Cycle, Master, Slave) and use Button 0 to transmit a packet
 * or blast packets using the RAIL library, where blast refers to a continuous
 * flow of packets. The current mode and application status is printed on the
 * LCD screen.
 * @copyright Copyright 2016 Silicon Laboratories, Inc. http://www.silabs.com
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rail.h"
#include "rail_types.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_emu.h"
#include "em_core.h"
#include "pti.h"
#include "pa.h"
#include "rtcdriver.h"
#include "bsp.h"
#include "spi.h"
#include "gpiointerrupt.h"
#include "rail_config.h"
#include "hal_common.h"
#include "railscallback.h"
#include "minmea/minmea.h"
#include "const.h"
#include "utils.h"
extern char trace[256];
extern char itrace ;

//
void MiscInit()
{
	// Initialize the chip
	CHIP_Init();
	// Initialize the system clocks and other HAL components
	halInit();
	CMU_ClockEnable(cmuClock_GPIO, true);
	CMU_ClockEnable(cmuClock_USART0, true);
	CMU_ClockEnable(cmuClock_USART1, true);
	CMU_ClockEnable(cmuClock_TIMER0, true);
	CMU_ClockEnable(cmuClock_TIMER1, true);
	// Initialize the BSP
	BSP_Init(BSP_INIT_BCC);
	GPIO_IntConfig(SPI_INT_PORT, SPI_INT_PIN, false /*risingEdge*/, true /*fallingEdge*/, true);
	GPIOINT_CallbackRegister(SPI_INT_PIN, SpiIntCallback);
	GPIO_IntEnable(1 << 21); // PB13 NSS PIN21	
	GPIO_PinModeSet(LED_PORT, LED_PIN, gpioModePushPull, 0);
	GPIO_PinModeSet(LED_PORTGREEN, LED_PINGREEN, gpioModePushPull, 0);
	GPIO_PinModeSet(CS_INT_PORT, CS_INT_PIN, gpioModePushPull, 0);
	GPIOINT_Init(); // gpio interrupt
	extern unsigned char mac[6];
	uint64_t mac64 = SYSTEM_GetUnique();
	mac [0] = mac64 >> 40;
	mac [1] = mac64 >> 32;
	mac [2] = mac64 >> 24;
	mac [3] = mac64 >> 16;
	mac [4] = mac64 >> 8;
	mac [5] = mac64;

	SetupSPI();
	SetupUART();
}
//

volatile long int q;
void del ()
{
	unsigned long int t;
	for (t = 0; t < 5000L; t++)
		q++;
}
volatile bool packetTx = true;
extern unsigned int _channel;
int cn;
int main(void)
{
	init_rs(); // RS Solomon init	
	MiscInit();
	InitRadio();
	changeRadioConfig(0);
	RAIL_TxPowerSet(200);
	StartReceive(_channel);
	RAIL_PaCtuneSet (0, 0); // TODO cfg it

	for (;;)
	{
		RAIL_TxData_t transmitPayload;
		unsigned char packetForSend[MAX_RF_SENDING_SIZE];
		unsigned char *pSpiPacket = NULL;
		unsigned int len = 0;
		if (((len = GetPacketForSend(&pSpiPacket)) != 0))
		{
			if (len <= MAX_RF_SENDING_SIZE)
			{
				len = MakeRadioPacketFromSpi(packetForSend, pSpiPacket, len);
				transmitPayload.dataPtr = packetForSend;
				transmitPayload.dataLength = len+10; /*no 10*/
				while (!IsTxDone())
					;
				RAIL_RfIdleExt(RAIL_IDLE_ABORT, true);
				RAIL_SetFixedLength(len);
				RAIL_TxDataLoad(&transmitPayload);
				SetSpiEvent(CC1310_EV_SEND_CONFIRMATION); // confirm sending
				PulseIntDelay2();
				del();
				StartTx(_channel);
				GPIO_PinOutToggle(LED_PORTGREEN, LED_PINGREEN); // debug
			}
				//GPIO_PinOutToggle(LED_PORT, LED_PIN); // debug
		}
		int toSendBySpi = 0;
		unsigned char tempSpiBuf[255];
		if ((toSendBySpi = rfReceivePacket(tempSpiBuf))!= 0) // new packet from air, already processed and handled
		{
			SetSpiEvent(CC1310_EV_RECV_RAW_PACKET); //flag received
			SendSpiData(tempSpiBuf, toSendBySpi);
			PulseIntDelay2();
			ReadRxPacket();
		//	GPIO_PinOutToggle(LED_PORTGREEN, LED_PINGREEN); // debug
		}
		// process GPS
		struct minmea_sentence_rmc frame;
		if (NMEAProcess(&frame) != 0)
		{
			// new GPS
			unsigned char tempGPSBuf[255];
			GPIO_PinOutToggle(LED_PORTGREEN, LED_PINGREEN); // debug
			len = MakeSpiNmeaPacketFromRmcframe(tempGPSBuf, frame);
			len += MakeRadioPacketFromSpi(packetForSend, tempGPSBuf,  len); // new (packetized length)
			// TO DO
			transmitPayload.dataPtr = packetForSend;
			transmitPayload.dataLength = len + 1; //add 1 byte of length
			RAIL_TxDataLoad(&transmitPayload);
			StartTx(0);
			while (!IsTxDone())
				; // now blocking waiting
		}

		if (!isReceiveMode() && !IsTxDone())
		{
			StartReceive(_channel);
		}

	}
}


