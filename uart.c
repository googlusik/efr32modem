#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rail.h"
#include "rail_types.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_emu.h"
#include "bsp.h"
#include "uartdrv.h"
#include "spidrv.h"
#include "gpiointerrupt.h"
#include "spi.h"
#include "uart.h"
#include "bspconfig.h"
#include "rail_config.h"
#include "hal_common.h"
#include "RS/dasmRS.h"
#include "uart.h"
#include "const.h"

DEFINE_BUF_QUEUE(64u, UartTxQueue);
DEFINE_BUF_QUEUE(64u, UartRxQueue);
UARTDRV_HandleData_t  UARTHandleData;
UARTDRV_Handle_t      UARTHandle = &UARTHandleData;
USART_TypeDef *uart = USART0;
char uartRxBuffer[128];
volatile char newMsg = 0;
volatile int uartRxPtr = 0;
char nmeaMessageWorkingCopy[128];
struct minmea_sentence_rmc frame;

 int NMEAProcess (struct minmea_sentence_rmc *frame)
 {
	 int result = 0;
		if (newMsg)
		{
			newMsg = 0;

			switch (minmea_sentence_id((char*)nmeaMessageWorkingCopy, false))
			{
			case MINMEA_SENTENCE_RMC:
			{
					if (minmea_parse_rmc(frame, (char*)nmeaMessageWorkingCopy))
					{
						return frame->valid;;

					}
					else
					{

					}
			} break;
			default:
				break;
			}

		}
		return result;
 }
void USART0_RX_IRQHandler(void)
{

	static char rxNMEANow = 0;
	if (!(uart->STATUS & USART_STATUS_RXDATAV))
		return;
		/* Reading out data */
	do
	{
		char recChar = USART_Rx(USART0);
		if (recChar == '$')
		{
			rxNMEANow = 1;
			uartRxPtr = 0;
			uartRxBuffer[uartRxPtr++] = recChar;

		}
		else if (rxNMEANow)
		{

			uartRxPtr %= sizeof (uartRxBuffer);
			uartRxBuffer[uartRxPtr++] = recChar;
			if (recChar == '\n') // end
			{
				rxNMEANow = 0;
				uartRxBuffer[uartRxPtr++] = 0;// must be zero ended
				if (newMsg == 0)
				{
						memcpy(nmeaMessageWorkingCopy, uartRxBuffer, uartRxPtr);
						newMsg = 1;
				}
				uartRxPtr = 0;
			}
		}
	} while (uart->STATUS & USART_STATUS_RXDATAV);
}

//
void SetupUART()
{

	UARTDRV_Init_t uartInit = USART_INIT_NMEA;
	UARTDRV_Init(UARTHandle, &uartInit);

	NVIC_ClearPendingIRQ(USART0_RX_IRQn);
	uartInit.port->IEN |= USART_IEN_RXDATAV;;
	uartInit.port->CMD = USART_CMD_CLEARTX;
	NVIC_ClearPendingIRQ(USART0_RX_IRQn);
	NVIC_EnableIRQ(USART0_RX_IRQn);
	USART_Enable(uart, usartEnable);
}



