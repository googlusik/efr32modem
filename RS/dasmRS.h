#ifndef dasmRS_h__
#define dasmRS_h__
// must be called once
void init_rs();
// adaptive encode variable length message (varLen == information len) 
// information len must be lower or EQ 223 bytes
// returns total encoded message length (will not exceed 255)
// messages lower  eq		will encode to len + (255- infoSz)
//						N <= 64 will encode + 8 par bytes ((inf.. 12.5 % overhead))
//						64 < N <= 128 will encode + 16 par bytes (25.. 12.5 % overhead)
//						up to 128 < N <= 223 will encode + 32 par bytes (25.. 14 % overhead)
// so msg lower than 8 bytes will have 100 % more overhead
int encodeVarLenMsgAdv(const unsigned char *src, unsigned char *dst, int varLen);
// decode variable length message (param total len)
int decodeVarLenMsgAdv(const unsigned char *src, unsigned char *dst, int totLen, int *infoLen);
#endif // dasmRS_h__
