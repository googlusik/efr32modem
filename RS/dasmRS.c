
#include <string.h>
#include "rs.h"
#include "dasmRS.h"
#include "rs255247.h"
#include "rs255223.h"
#include "rs255239.h"

#undef NN
#define NN 255
#define MAX_DATA_SZ 223
//////////////////////////////////////////////////////////////////////////
void init_rs()
{
	init_rs223();
	init_rs239();
	init_rs247();
}
//////////////////////////////////////////////////////////////////////////
struct RS
{
	int(*pEnc) (dtype *, dtype *);
	int(*pDec) (dtype *, int *, int);
	int tresholdInfo;
	int infoSz;
};
//////////////////////////////////////////////////////////////////////////
static struct RS rs[3] = { /*ascending  treshld order only*/
	{encode_rs247, eras_dec_rs247, 64, 247},
	{ encode_rs239, eras_dec_rs239, 128, 239 },
	{ encode_rs223, eras_dec_rs223, 255, 223 },
};

//////////////////////////////////////////////////////////////////////////
int encodeVarLenMsgAdv(const unsigned char *src, unsigned char *dst, int varLen)
{
	if (varLen > MAX_DATA_SZ) return 0;
	unsigned char temp[NN];
	int totLen;
	memset(temp, 0, sizeof(temp));
	memcpy(temp, src, varLen);
	int i;
	for (i = 0; i < sizeof(rs) / sizeof (rs[0]); i++)
	{
		if (varLen <= rs[i].tresholdInfo)
		{
			rs[i].pEnc(temp, &temp[rs[i].infoSz]);
			memcpy(dst, src, varLen);
			memcpy(&dst[varLen], &temp[rs[i].infoSz], NN - rs[i].infoSz);
			break;
		}
	}
	return varLen + (NN - rs[i].infoSz);
}
//////////////////////////////////////////////////////////////////////////
int GetInfoLenFromFullLength(int len)
{
	int i; int parLen;
	for (i = 0; i < sizeof(rs) / sizeof (rs[0]); i++)
	{
		parLen = NN - rs[i].infoSz;
		if (len <= rs[i].tresholdInfo + parLen)
			return (len - parLen) > 0 ? (len - parLen) : 0;
	}
	return 0;
}
int decodeVarLenMsgAdv(const unsigned char *src, unsigned char *dst, int totLen, int *infoLen)
{
	unsigned char temp[NN];
	int corrected;
	int varLen = GetInfoLenFromFullLength(totLen);
	if (infoLen != NULL)
		*infoLen = varLen;
	if ((varLen == 0) || (varLen > MAX_DATA_SZ)) return 0;
	memset(temp, 0, sizeof(temp));
	memcpy(temp, src, varLen);
	int i;
	for (i = 0; i < sizeof(rs) / sizeof (rs[0]); i++)
	{
		if (varLen <= rs[i].tresholdInfo)
		{
			memcpy(&temp[rs[i].infoSz], &src[varLen], NN -  rs[i].infoSz);
			corrected = rs[i].pDec(temp, NULL, 0); // no erase loc known
			memcpy(dst, temp, varLen);
			break;
		}
	}
	return corrected;
}

//int decodeVarLenMsg(const unsigned char *src, unsigned char *dst, int varLen)
//{
//	unsigned char temp[NN];
//	int corrected;
//	memset(temp, 0, sizeof(temp));
//	memcpy(temp, src, varLen);
//	if (varLen <= 127)
//	{
//		memcpy(&temp[KK239], &src[varLen], NN - KK239);
//		corrected = eras_dec_rs239(temp, NULL, 0); // no erase loc known
//		memcpy(dst, temp, varLen);
//	}
//	else
//	{
//		memcpy(&temp[KK223], &src[varLen], NN - KK223);
//		corrected = eras_dec_rs223(temp, NULL, 0); // no erase loc known
//		memcpy(dst, temp, varLen);
//	}
//	return corrected;
//}
//int encodeVarLenMsg(const unsigned char *src, unsigned char *dst, int varLen)
//{
//
//	unsigned char temp[NN];
//	memset(temp, 0, sizeof(temp));
//	memcpy(temp, src, varLen);
//	if (varLen <= 127)
//	{
//		encode_rs239(temp, &temp[KK239]);
//		memcpy(dst, src, varLen);
//		memcpy(&dst[varLen], &temp[KK239], NN - KK239);
//		return varLen + NN - KK239;
//	}
//	else
//	{
//		encode_rs223(temp, &temp[KK223]);
//		memcpy(dst, src, varLen);
//		memcpy(&dst[varLen], &temp[KK223], NN - KK223);
//		return varLen + NN - KK223;
//	}
//}

